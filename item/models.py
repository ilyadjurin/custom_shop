from django.contrib.auth.models import AbstractUser as DjangoAbstractUser, User
from django.db import models


class Item(models.Model):
    dttm_created = models.DateTimeField(auto_now_add=True)
    dttm_deleted = models.DateTimeField(null=True, blank=True)

    price = models.FloatField()
    name = models.CharField(max_length=16, default='', blank=True)
    details = models.CharField(max_length=1024, null=True, blank=True)

    class Meta:
        verbose_name = "item"
        verbose_name_plural = "items"
        ordering = ['id']


class MarketingCampaign(models.Model):
    dttm_created = models.DateTimeField(auto_now_add=True)
    dttm_deleted = models.DateTimeField(null=True, blank=True)

    dttm_start = models.DateTimeField()
    dttm_end = models.DateTimeField()

    name = models.CharField(max_length=16, )

    class Meta:
        verbose_name = "marketing campaign"
        verbose_name_plural = "marketing campaigns"
        ordering = ['id']


class Client(DjangoAbstractUser):
    SEX_FEMALE = 'F'
    SEX_MALE = 'M'
    SEX_CHOICES = (
        (SEX_FEMALE, 'Female',),
        (SEX_MALE, 'Male',),
    )

    sex = models.CharField(max_length=1, choices=SEX_CHOICES, default=SEX_MALE)
    campaign = models.ForeignKey(MarketingCampaign, on_delete=models.CASCADE, null=True, related_name='client')
    bought_items = models.ManyToManyField(Item, null=True, related_name='clients')
