from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import gettext_lazy as _

from item.models import Client, Item, MarketingCampaign


class CustomUserAdmin(UserAdmin):
    fieldsets = (
        (None, {'fields': ('username', 'password', 'sex', 'campaign', 'bought_items')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'email')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    list_display = ('username', 'email', 'sex', 'first_name', 'last_name', 'is_staff')


admin.site.register(Client, CustomUserAdmin)


@admin.register(Item)
class ItemAdmin(admin.ModelAdmin):
    model = Item


@admin.register(MarketingCampaign)
class MarketingCampaignAdmin(admin.ModelAdmin):
    model = MarketingCampaign
