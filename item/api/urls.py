from django.urls import path
from item.api.views import ItemListView, ItemSumView

urlpatterns = [
    path('list_item', ItemListView.as_view()),
    path('list_sum', ItemSumView.as_view()),
]
