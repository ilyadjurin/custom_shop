import datetime

from rest_framework import serializers

from item.models import Item, Client


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = '__all__'


class ItemListSerializer(serializers.ModelSerializer):
    clts = Client.objects.filter(
        date_joined__lte=datetime.datetime.strptime('2019-06-01T15:37:36', '%Y-%m-%dT%H:%M:%S'))
    clients = ClientSerializer(clts, many=True, read_only=True, )

    class Meta:
        model = Item
        fields = ("id",
                  "dttm_created",
                  "dttm_deleted",
                  "price",
                  "name",
                  "details",
                  "clients")

    def to_representation(self, instance):
        data = super(ItemListSerializer, self).to_representation(instance)
        buy_amount = len(data['clients'])
        data.popitem(last=True)
        data['buy_amount'] = buy_amount
        return data


class ItemViewSerializer(serializers.Serializer):
    ids = serializers.ListField()
