from django.db.models import Sum
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import status
from rest_framework.generics import ListAPIView
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView

from item.models import Item
from .serializers import ItemListSerializer, ItemViewSerializer


class ItemListView(ListAPIView):
    """
    List item vs filters
    """
    permission_classes = (AllowAny,)

    filter_backends = (DjangoFilterBackend,)
    filterset_fields = ('price', 'clients__sex')

    queryset = Item.objects.all()
    serializer_class = ItemListSerializer


class ItemSumView(APIView):
    """
        List sum of product for user
    """
    permission_classes = (AllowAny,)
    queryset = Item.objects.all()

    def post(self, request, *args, **kwargs):
        input_serializer = ItemViewSerializer(data=request.data)
        input_serializer.is_valid(raise_exception=True)

        items = Item.objects.filter(clients__id__in=input_serializer.validated_data['ids'])
        response = {"count": len(items), "price_sum": items.aggregate(Sum('price'))['price__sum']}

        return Response({"response": response}, status=status.HTTP_200_OK)

